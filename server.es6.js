'use strict';

// server.js

// set up ======================================================================
const express  = require('express')
    , app      = express()
    , port     = process.env.PORT || 8080
    , mongoose = require('mongoose')
    , passport = require('passport')
    , flash    = require('connect-flash')

    , morgan       = require('morgan')
    , cookieParser = require('cookie-parser')
    , bodyParser   = require('body-parser')
    , session      = require('express-session')

    , configDB = require('./config/database.js');

// configuration ===============================================================
mongoose.connect(configDB.url);

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
// get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// static routes
// so we can use /bower_components instead of ../bower_components in views
app.use('/bower_components',  express.static(__dirname + '/bower_components'));

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({ 
  secret: 'thisismysecretkeyforjuicysessiondata',
  resave: false,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./app/routes.js')(app, passport);

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);

