## Contributing Guide

### git

Use the `git-flow` process. [cheatsheet](http://danielkummer.github.io/git-flow-cheatsheet)

Make use of pull requests.

### Code Style

* 2 spaces for tabs, no tab characters for indenting
* Follow the guidelines outlined in the `.jshintrc`
* Style as far as ES6 and beyond can be discussed as adopted
