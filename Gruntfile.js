'use strict';

// Gruntfile.js
module.exports = function (grunt) {

  // Replace repeated calls to grunt.loadNpmTasks(...)
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({

    // configure nodemon
    nodemon: {
      dev: {
        script: 'server.js'
      }
    },

    // Transpiler
    '6to5': {
      options: {
        sourceMap: false
      },
      dist: {
        files: [{
          expand: true,
          cwd: './',
          src: [
           'server.es6.js',
            'app/**/*.es6.js',
            'config/**/*.es6.js'
          ],
          dest: './',
          ext: '.js'
        }]
      }
    },

    // Make our 6to5 js output pretty again
    'jsbeautifier' : {
      files : [
        '!Gruntfile.js',
        '!server.es6.js',
        'server.js',
        'app/**/*.js',
        'config/**/*.js',
        // Ignore *.es6.js
        '!app/**/*.es6.js',
        '!config/**/*.es6.js'
      ],
      options: {
        js : {
          braceStyle: 'collapse',
          breakChainedMethods: false,
          e4x: false,
          evalCode: false,
          indentChar: ' ',
          indentLevel: 0,
          indentSize: 2,
          indentWithTabs: false,
          jslintHappy: false,
          keepArrayIndentation: false,
          keepFunctionIndentation: false,
          maxPreserveNewlines: 10,
          preserveNewlines: true,
          spaceBeforeConditional: true,
          spaceInParen: false,
          unescapeStrings: false,
          wrapLineLength: 0
        }
      }
    }

  });

  grunt.registerTask('default', ['6to5', 'jsbeautifier']);
};
